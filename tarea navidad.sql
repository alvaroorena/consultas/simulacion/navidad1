﻿SELECT c.nombre,c.dorsal 
  FROM ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal INNER JOIN equipo e1 ON c.nomequipo = e1.nomequipo 
  WHERE e1.director="Bill Gates";

SELECT c.nombre,c.dorsal 
  FROM ciclista c 
  WHERE c.dorsal IN(SELECT e.dorsal FROM etapa e INNER JOIN ciclista c1 ON e.dorsal = c1.dorsal INNER JOIN equipo e1 ON c1.nomequipo = e1.nomequipo 
  WHERE e1.director="Bill Gates");

